package mysql

import (
	"fmt"
	"time"

	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
)

type DB struct {
	DB     *sqlx.DB
	Config *DBConfig
}

type DBConfig struct {
	DBUser       string
	DBPassword   string
	DBHost       string
	DBPort       string
	DBName       string
	MaxIdleConns int
	MaxOpenConns int

	ConnMaxLifetime int
}

func NewDB(config *DBConfig) *DB {
	return &DB{
		Config: config,
	}
}

func (d *DB) Connect() (*sqlx.DB, error) {
	var (
		dataSourceName string
		err            error
	)

	dataSourceName = fmt.Sprintf(
		"%s:%s@(%s:%s)/%s",
		d.Config.DBUser,
		d.Config.DBPassword,
		d.Config.DBHost,
		d.Config.DBPort,
		d.Config.DBName,
	)

	if d.DB, err = sqlx.Connect("mysql", dataSourceName); err != nil {
		return d.DB, err
	}

	d.DB.SetMaxIdleConns(d.Config.MaxIdleConns)
	d.DB.SetMaxOpenConns(d.Config.MaxOpenConns)
	d.DB.SetConnMaxLifetime(time.Duration(d.Config.ConnMaxLifetime) * time.Second)

	return d.DB, nil
}
