package consul

import (
	"fmt"

	"github.com/hashicorp/consul/api"
)

type Consul struct {
	Config  *Settings
	Client  *api.Client
	Catalog *api.Catalog
	Health  *api.Health
	Agent   *api.Agent
}

type Settings struct {
	Addresss      string
	Token         string
	DataCenter    string
	SkipTLSVerify bool
}

type Service struct {
	ID      string
	Name    string
	Port    int
	Address string
	Tags    []string
	Meta    map[string]string
}

type CatalogService struct {
	*Service
	Health string
}

type ServiceSettings struct {
	Service     *Service
	HealthCheck *HKSettings
}

type HKSettings struct {
	Name            string
	HTTP            string
	Timeout         string
	Interval        string
	DeregisterAfter string
	SkipTLSVerify   bool
	Status          string // позволяет изначально инициализировать сервис живым
}

type Nodes struct {
	Capis    []*CatalogService
	Aapis    []*CatalogService
	Monitors []*CatalogService
}

type Consuler interface {
	Connect() (*Consul, error)
	GetService(string, string) ([]*CatalogService, error)
	RegisterService(*ServiceSettings) error
	DeregisterService(string) error
	getAgent() *api.Agent
	getCatalog() *api.Catalog
}

func New(config *Settings) *Consul {
	return &Consul{
		Config: config,
	}
}

func (c *Consul) Connect() (*Consul, error) {
	var err error

	if c.Client, err = api.NewClient(&api.Config{
		Address: c.Config.Addresss,
		Token:   c.Config.Token,
		TLSConfig: api.TLSConfig{
			InsecureSkipVerify: c.Config.SkipTLSVerify,
		},
	}); err != nil {
		return nil, err
	}

	c.Agent = c.getAgent()
	c.Catalog = c.getCatalog()
	c.Health = c.getHealth()

	return c, nil
}

func (c *Consul) getAgent() *api.Agent {
	return c.Client.Agent()
}

func (c *Consul) getCatalog() *api.Catalog {
	return c.Client.Catalog()
}

func (c *Consul) getHealth() *api.Health {
	return c.Client.Health()
}

func (c *Consul) IsExists(groupName, id string) (bool, error) {
	var (
		item []*api.CatalogService
		err  error
	)

	if item, _, err = c.Catalog.Service(groupName, "", &api.QueryOptions{
		Filter: fmt.Sprint("ServiceID==\"", id, "\""),
	}); err != nil {
		return false, err
	}

	if len(item) == 0 {
		return false, nil
	}

	return true, nil
}

func (c *Consul) RegisterService(settings *ServiceSettings) error {
	var err error

	if err = c.Agent.ServiceRegisterOpts(&api.AgentServiceRegistration{
		ID:      settings.Service.ID,
		Name:    settings.Service.Name,
		Port:    settings.Service.Port,
		Address: settings.Service.Address,
		Tags:    settings.Service.Tags,
		Meta:    settings.Service.Meta,
		Check: &api.AgentServiceCheck{
			Name:                           settings.HealthCheck.Name,
			HTTP:                           settings.HealthCheck.HTTP,
			Timeout:                        settings.HealthCheck.Timeout,
			Interval:                       settings.HealthCheck.Interval,
			DeregisterCriticalServiceAfter: settings.HealthCheck.DeregisterAfter,
			TLSSkipVerify:                  settings.HealthCheck.SkipTLSVerify,
			Status:                         settings.HealthCheck.Status,
		},
	}, api.ServiceRegisterOpts{ReplaceExistingChecks: true}); err != nil {
		return err
	}

	return nil
}

func (c *Consul) GetService(groupName, tag string) ([]*CatalogService, error) {
	var (
		services []*CatalogService
		items    []*api.CatalogService
		health   string
		err      error
	)

	if items, _, err = c.Catalog.Service(groupName, tag, nil); err != nil {
		return nil, err
	}

	for _, v := range items {
		if health, err = c.GetValidCheck(v.ServiceName, v.ServiceID); err != nil {
			return nil, err
		}

		services = append(services, &CatalogService{
			Health: health,
			Service: &Service{
				ID:      v.ServiceID,
				Name:    v.ServiceName,
				Address: v.ServiceAddress,
				Port:    v.ServicePort,
				Tags:    v.ServiceTags,
				Meta:    v.ServiceMeta,
			},
		})
	}

	return services, nil
}

func (c *Consul) DeregisterService(id string) error {
	var err error

	if err = c.Agent.ServiceDeregister(id); err != nil {
		return err
	}

	return nil
}

func (c *Consul) GetValidCheck(name, id string) (string, error) {
	var (
		services []*api.ServiceEntry
		health   string
		err      error
	)

	if services, _, err = c.Health.Service(name, "", false, &api.QueryOptions{
		Filter: fmt.Sprint("Checks.ServiceID==\"", id, "\""),
	}); err != nil {
		return "", err
	}

	if len(services) > 0 {
		for _, v := range services[0].Checks {
			if v.CheckID == fmt.Sprint("service:", id) {
				health = v.Status
			}
		}
	} else {
		return "", fmt.Errorf("health check not found for service %s:%s", name, id)
	}

	return health, nil
}
