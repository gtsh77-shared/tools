package net

import (
	"fmt"
	"net"
)

func NetAddrOfInterface(name string, idx ...int) (net.IP, error) {
	var (
		ifaces []net.Interface
		addrs  []net.Addr
		addr   net.IP
		index  int
		err    error
	)

	if ifaces, err = net.Interfaces(); err != nil {
		return nil, err
	}

	for _, i := range ifaces {
		if i.Name == name {
			if addrs, err = i.Addrs(); err != nil {
				return nil, err
			}
			break
		}
	}

	if len(addrs) == 0 {
		return nil, fmt.Errorf("no ipv4 found on %s interface", name)
	}

	if len(idx) > 0 {
		index = idx[0]
	}

	if addr, _, err = net.ParseCIDR(addrs[index].String()); err != nil {
		return nil, err
	}

	return addr, nil

}
