package metadata

import (
	"context"
	"fmt"
	"time"

	"google.golang.org/grpc/metadata"
)

const (
	SFUID    = "sfu_resource_id"
	AMIXERID = "amixer_resource_id"
)

type Ctx struct {
	Ctx    context.Context
	Cancel context.CancelFunc
}

func NewMetaCtx(duration time.Duration) *Ctx {
	var (
		ctx    context.Context
		cancel context.CancelFunc
	)

	ctx, cancel = context.WithTimeout(context.Background(), duration)
	return &Ctx{
		Ctx:    ctx,
		Cancel: cancel,
	}
}

func MetaFromCtx(ctx context.Context) *Ctx {
	return &Ctx{
		Ctx: ctx,
	}
}

func (c *Ctx) With(key, val string) *Ctx {
	var meta metadata.MD = metadata.Pairs(key, val)
	c.Ctx = metadata.NewOutgoingContext(c.Ctx, meta)

	return c
}

func (c *Ctx) Get(key string) (string, error) {
	var (
		id   string
		meta metadata.MD
		v    []string
		ok   bool
	)

	if meta, ok = metadata.FromIncomingContext(c.Ctx); !ok {
		return "", fmt.Errorf("no meta data provided")
	} else {
		if v, ok = meta[key]; ok {
			if len(v) > 0 {
				id = v[0]
			} else {
				return "", fmt.Errorf("meta data field %s missing", key)
			}
		}
	}

	return id, nil
}
