package migrate

import (
	"errors"
	"fmt"

	"github.com/golang-migrate/migrate/v4"
	"github.com/golang-migrate/migrate/v4/database"
	pg "github.com/golang-migrate/migrate/v4/database/postgres"
	"github.com/jmoiron/sqlx"

	_ "github.com/golang-migrate/migrate/v4/source/file"
	_ "github.com/lib/pq"
)

type Migration struct {
	Mig  *migrate.Migrate
	Path string
}

type Migrationer interface {
	WithPath(string) (*Migration, error)
	WithPGInstance(*sqlx.DB) (*Migration, error)
	Version() (uint, bool, error)
	Up() error
	Down() error
	Steps(int) error
}

func NewMigration() *Migration {
	return &Migration{}
}

func (m *Migration) WithPath(path string) *Migration {
	m.Path = path
	return m
}

func (m *Migration) WithPGInstance(dbx *sqlx.DB) (*Migration, error) {
	var (
		path   string = fmt.Sprint("file://", m.Path)
		driver database.Driver
		err    error
	)

	if driver, err = pg.WithInstance(dbx.DB, &pg.Config{}); err != nil {
		return m, err
	}

	if m.Mig, err = migrate.NewWithDatabaseInstance(path, "postgres", driver); err != nil {
		return m, err
	}

	return m, nil
}

func (m *Migration) Version() (bool, uint, bool, error) {
	var (
		version uint
		dirty   bool
		err     error
	)

	if version, dirty, err = m.Mig.Version(); err != nil {
		if errors.Is(err, migrate.ErrNilVersion) {
			return true, 0, false, err
		}

		return false, 0, false, err
	}

	return false, version, dirty, err
}

func (m *Migration) Up() error {
	return m.Mig.Up()
}

func (m *Migration) Down() error {
	return m.Mig.Down()
}

func (m *Migration) Steps(n int) error {
	return m.Mig.Steps(n)
}
